#!/usr/bin/env python
# vim: et st=4 sts=4 ts=4:
#
# Batch file renamer
# Copyright (C) 2007  Sebastian Nowicki
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
# 
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import os, sys, string
from optparse import OptionParser

import re

class Action:
    """Action holding a callback and arguments"""
    def __init__(self):
        self.callback = None
        self.arguments = None
        pass

    def set_callback(self, callback):
        self.callback = callback
    
    def set_arguments(self, arguments):
        self.arguments = arguments

    def add_arguments(self, arguments):
        self.arguments.update(arguments)

    def execute(self):
        if self.arguments:
            return self.callback(**self.arguments)
        else:
            return self.callback()


class ActionContainer:
    """Container for various actions"""

    def __init__(self):
        self.__actions = []
        self.__count = 0
        self.__index = 0

    def __iter__(self):
        return self

    def __getitem__(self, index):
        return self.__actions[index]

    def __len__(self):
        return self.__count

    def next(self):
        if self.__index == self.__count:
            self.__index = 0
            raise StopIteration
        action = self.__actions[self.__index]
        self.__index += 1
        return action

    def add(self, action):
        self.__actions.append(action)
        self.__count += 1


def regex_replace(**kwargs):
    result = []
    for source in sources:
        result.append(re.sub(kwargs['pattern'], kwargs['replace'], source))
    return result

def prefix(**kwargs):
    result = []
    for source in sources:
        result.append(kwargs['prefix'] + source)
    return result

def suffix(**kwargs):
    result = []
    for source in sources:
        result.append(source + kwargs['suffix'])
    return result

def case_title(**kwargs):
    result = []
    for source in sources:
        result.append(source.title())
    return result

def case_lower(**kwargs):
    result = []
    for source in sources:
        result.append(source.lower())
    return result

def case_upper(**kwargs):
    result = []
    for source in sources:
        result.append(source.upper())
    return result

def opts_add_action(option, opt_str, value, parser):
    action = Action()
    if opt_str == "-r":
        action.set_callback(regex_replace)
        action.set_arguments(dict(pattern=value[0], replace=value[1]))
    elif opt_str == "-p":
        action.set_callback(prefix)
        action.set_arguments(dict(prefix=value))
    elif opt_str == "-s":
        action.set_callback(suffix)
        action.set_arguments(dict(suffix=value))
    elif opt_str == "-T":
        action.set_callback(case_title)
    elif opt_str == "-l":
        action.set_callback(case_lower)
    elif opt_str == "-u":
        action.set_callback(case_upper)
    actions.add(action)

if __name__ == "__main__":
    VERSION = "0.2"
    actions = ActionContainer()
    sources = []

    parser = OptionParser(usage="Usage: %prog [options] FILE ...",
            version="%prog " + VERSION)
    # Add case insensitivity option
    parser.add_option("-i", "--case-insensitive",
            action="store_true", dest="case_insensitive", 
            help="Filename case is disregarded when matching")
    # Add regular expression options
    parser.add_option("-r",
            action="callback", callback=opts_add_action,
            metavar="PATTERN REPLACE", type="string", nargs=2,
            help="Search for PATTERN and replace with REPLACE")
    # Prefix
    parser.add_option("-p", "--prefix",
            action="callback", callback=opts_add_action, metavar="STRING",
            type="string", help="Prefix file names with STRING")
    # Suffix
    parser.add_option("-s", "--suffix",
            action="callback", callback=opts_add_action, metavar="STRING",
            type="string", help="Suffix file names with STRING")
    # Title Case
    parser.add_option("-T", "--title-case",
            action="callback", callback=opts_add_action,
            help="Title Case File Names Like This")
    # Lower case
    parser.add_option("-l", "--lower-case",
            action="callback", callback=opts_add_action,
            help="Make file names lower case")
    # Upper case
    parser.add_option("-u", "--upper-case",
            action="callback", callback=opts_add_action,
            help="Make file names upper case")
    # Test only
    parser.add_option("-t", "--test",
            action="store_true", dest="test",
            help="Do not rename, just show what would happen",)
    # Verbose
    parser.add_option("-v", "--verbose",
            action="store_true", help="Be verbose", dest="verbose",)
    # Parse command line arguments
    options, args = parser.parse_args()

    sources = args

    if len(sys.argv) == 1:
        parser.error("one or more FILEs are required")
        sys.exit(1)

    # mv functionality
    elif len(args) == 2 and len(actions) == 0:
        print "%s --> %s" % (sources[0], sources[1])
    else:
        for action in actions:
            sources = action.execute()

    if options.test:
        for index in range(0, len(args)):
            print "%s --> %s" % (args[index], sources[index])
    else:
        for index in range(0, len(args)):
            if options.verbose:
                print "%s --> %s" % (args[index], sources[index])
            os.renames(args[index], sources[index])
